(function () {
  //===== Prealoder

  window.onload = function () {
    window.setTimeout(fadeout, 500);
  };

  function fadeout() {
    document.querySelector(".preloader").style.opacity = "0";
    document.querySelector(".preloader").style.display = "none";
  }

  /*=====================================
    Sticky
    ======================================= */
  window.onscroll = function () {
    const header_navbar = document.querySelector(".navbar-area");
    const sticky = header_navbar.offsetTop;
    const logo = document.querySelector(".navbar-brand img");

    if (window.pageYOffset > sticky) {
      header_navbar.classList.add("sticky");
      logo.src = "assets/img/logo/logo-2.svg";
    } else {
      header_navbar.classList.remove("sticky");
      logo.src = "assets/img/logo/logo.svg";
    }

    // show or hide the back-top-top button
    const backToTo = document.querySelector(".scroll-top");
    if (
      document.body.scrollTop > 50 ||
      document.documentElement.scrollTop > 50
    ) {
      backToTo.style.display = "flex";
    } else {
      backToTo.style.display = "none";
    }
  };

  // for menu scroll
  const pageLink = document.querySelectorAll(".page-scroll");

  pageLink.forEach((elem) => {
    elem.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(elem.getAttribute("href")).scrollIntoView({
        behavior: "smooth",
        offsetTop: 1 - 60,
      });
    });
  });

  // section menu active
  function onScroll(event) {
    const sections = document.querySelectorAll(".page-scroll");
    const scrollPos =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop;

    for (let i = 0; i < sections.length; i++) {
      const currLink = sections[i];
      const val = currLink.getAttribute("href");
      const refElement = document.querySelector(val);
      const scrollTopMinus = scrollPos + 73;
      if (
        refElement.offsetTop <= scrollTopMinus &&
        refElement.offsetTop + refElement.offsetHeight > scrollTopMinus
      ) {
        document.querySelector(".page-scroll").classList.remove("active");
        currLink.classList.add("active");
      } else {
        currLink.classList.remove("active");
      }
    }
  }

  window.document.addEventListener("scroll", onScroll);

  //===== close navbar-collapse when a  clicked
  let navbarToggler = document.querySelector(".navbar-toggler");
  const navbarCollapse = document.querySelector(".navbar-collapse");

  document.querySelectorAll(".page-scroll").forEach((e) =>
    e.addEventListener("click", () => {
      navbarToggler.classList.remove("active");
      navbarCollapse.classList.remove("show");
    })
  );
  navbarToggler.addEventListener("click", function () {
    navbarToggler.classList.toggle("active");
  });

  // WOW active
  new WOW().init();

  // send data to airtable
  var form = document.getElementById('form-download');
  form.addEventListener('submit',function(event){
    event.preventDefault();
    var phone = document.getElementById('phone').value;
    fetch('https://api.airtable.com/v0/appWRwCx4kq3lVk4P/Leads',{
      method: 'POST',
      body: JSON.stringify({
        "records": [
          {
            "fields": {
              "Phone": phone,
            }
          }
        ]
      }),
      headers: {
        'Authorization': 'Bearer key42UEl3fueE5JqS',
        'Content-Type': 'application/json'
      }
    })
        .then(response => response.json())
        .then(data => {
          // console.log('Success:', data);
          document.getElementById('phone').value = ''
        })
        .catch((error) => {
          // console.error('Error:', error);
        });
  })

  // send data to airtable
  var email_form = document.getElementById('email-form');
  email_form.addEventListener('submit',function(event){
    event.preventDefault();
    var email = document.getElementById('email').value;
    fetch('https://api.airtable.com/v0/appWRwCx4kq3lVk4P/Subscribe',{
      method: 'POST',
      body: JSON.stringify({
        "records": [
          {
            "fields": {
              "Email": email,
            }
          }
        ]
      }),
      headers: {
        'Authorization': 'Bearer key42UEl3fueE5JqS',
        'Content-Type': 'application/json'
      }
    })
        .then(response => response.json())
        .then(data => {
          // console.log('Success:', data);
          document.getElementById('email').value = ''
        })
        .catch((error) => {
          // console.error('Error:', error);
        });
  })
})();
